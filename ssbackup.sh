#!/bin/sh

# Write a script that, when executed from the home directory of an account via the command line:
# Creates ~/SecretSourceBackups if it doesn't exist
# Creates a backup folder inside ~/SecretSourceBackups with the date and time as the name, e.g.: 2015-08-24_23-17-11
# Copies the web root to the backup folder
# Gets the MySQL credentials from wp-config and tests the db connection
# Dumps the database to the web folder
# Makes sure the database backup file is of a certain, minimum size and contains the minimum WP tables (do some minimum sanity checks)
# Compresses the backup folder to save space

# The above script will be called ss_backup.sh. It will take no parameters but must be run from the account home (a.k.a. ~/). It expects to find a web root called public_html. 
 
# set a bunch of variables
for FN in "$@"
do
	case "$FN" in
		'--migrate')
			shift
			DO_MIGRATION='true'
		;;
		'--documentroot')
			shift
			DOC_ROOT="${1:-public_html}"
		;;
		'--skip-uploads')
			shift
			SKIP_UPLOADS='true'
		;;
		'--tail')
			shift
			TAIL="${1}"
		;;
	esac
done

# set some environment variables
THIS_DIR=$( pwd )
BACKUP_DIR="$THIS_DIR/SecretSourceBackups"
PUBLIC_HTML="${DOC_ROOT:=public_html}"
mkdir -p "$BACKUP_DIR"
DATEFILE='%Y-%m-%d_%H%M%S'
BACKUP_NAME=$(echo "backup_"`date +$DATEFILE`)
WP_CONFIG="$THIS_DIR/$PUBLIC_HTML/wp-config.php"
DB_BACKUP_FILE="$THIS_DIR/$PUBLIC_HTML/$BACKUP_NAME.sql"
MYSQLDCOMMAND="$BACKUP_DIR/mysqldump.sh"

#==== START set some platform specific variables ====#
  exec 3>&2
  exec 2> /dev/null
  # figure out whether to use --summarize or -d
  du -d0 ~/test/ > /dev/null
  if [ "0" == $? ]
  then
      # Mac OS X
      DEPTH='-d0'
  else
      # GNU
      DEPTH='--summarize'
  fi

  # figure out whether to use -d or -v and days or d
  date -v -10d > /dev/null
  if [ "0" == $? ]
  then
      # Mac OS X
      DATE_PARAM='-v'
      DATE_DAYS='d'
  else
      # GNU
      DATE_PARAM='-d'
      DATE_DAYS=' days'
  fi
  
  # figure out how to call wp-cli.phar, if possible
  wp-cli.phar cli version > /dev/null
  if [ "0" == $? ]
  then
      # it's installed and callable
      WPCMD='wp-cli.phar'
      $WPCMD cli version >> "$BACKUP_DIR/backup_error.log"
      $WPCMD cli info >> "$BACKUP_DIR/backup_error.log"
  else
      # it's not installed, apparently, 
      # so some things won't work too well
      WPCMD=0
  fi
  exec 2>&3-
#==== END set some platform specific variables ====#


# make sure wp-config.php actually exists before doing anything else
if [ -f "$WP_CONFIG" ]
then
	echo "Found the wp-config.php file. This is good!"
	# add prompts for migration
	if [ 'true' == "$DO_MIGRATION" ]
	then
	    # is wp-cli.phar a supported version?
	    if [ -e $WPISEXECUTABLE ]
	    then
	        WPCMDVERSION=$($WPCMD cli version)
	        WPCMDMAJORVERSION=$(echo $WPCMDVERSION | cut -d . -f 1 \
	            | cut -d' ' -f 2)
	        WPCMDMINORVERSION=$(echo $WPCMDVERSION | cut -d . -f 2)
	        case $WPCMDMAJORVERSION in
	            0)
	                if (( $WPCMDMINORVERSION < 24 ))
	                then
	                    echo 'Sorry, the minimum supported version of '
	                    echo 'wp-cli is 0.24.1. You have '$WPCMDVERSION
	                    echo 'Using grep to do search and replace, '
	                    echo 'among other things (not good)'
	                    WPISSUPPORTED=false
	                else
	                    echo "wp-cli is a supported version "
	                    echo "($WPCMDVERSION), continuing..."
	                    WPISSUPPORTED=true
	                fi
	            ;;
	            *)
                    WPISSUPPORTED=true
	            ;;
	        esac
	    else
	        echo "$WPCMD is installed but does not appear to be executable."
	    fi
		
		# get the new domain name
		# and please, bash gurus, how can I shorten this line to 72 chr?
		read -p 'Please enter the new Home URL [ex.: http://www.mynewsite.com]. Be sure to include the trailing slash! : ' NEW_HOME_URL
		if [ "" == "$NEW_HOME_URL" ]
		then
			DO_MIGRATION='false'
			echo 'The Home URL cannot be blank. Proceeding without '
			echo 'preparing for a migration.'
		else
			echo "You entered $NEW_HOME_URL. What is the new Site URL?"
			read -p "Please enter the new Site URL [$NEW_HOME_URL]: " NEW_SITE_URL
			if [ "" == "$NEW_SITE_URL" ]
			then
				NEW_SITE_URL="$NEW_HOME_URL"
				echo "Using the new Home URL ($NEW_HOME_URL) as the new"
				echo "Site URL."
			fi
			echo "The new Site URL is $NEW_SITE_URL."
			read -p "Please enter the new Site path. The current path is $THIS_DIR/$PUBLIC_HTML: " NEW_SITE_PATH
			if [ "" == "$NEW_SITE_PATH" ]
			then
				NEW_SITE_PATH="$THIS_DIR/$PUBLIC_HTML"
				echo "Using the current path ($THIS_DIR/$PUBLIC_HTML) "
				echo "as the new site path."
			fi
			if [ ! -d "$NEW_SITE_PATH" ]
			then
				echo "We're sorry. $NEW_SITE_PATH does not seem to be a"
				echo "valid path. Continuing anyway as the new path may"
				echo "be on a new server..."
			fi
			echo "The new site path is $NEW_SITE_PATH."
		fi
	fi
	
	# get the DB config from wp-config.php
	DB_NAME=$(grep -o -E '^[[:space:]]*define.+?DB_NAME.+?,[[:space:]]*.+?[a-zA-Z_][a-zA-Z_0-9\-]*' "$WP_CONFIG" | cut -d"'" -f 4)
	DB_USER=$(grep -o -E '^[[:space:]]*define.+?DB_USER.+?,[[:space:]]*.+?[a-zA-Z_][a-zA-Z_0-9\-]*' "$WP_CONFIG" | cut -d"'" -f 4)
	DB_PASS=$(grep -o -E '^[[:space:]]*define.+?DB_PASSWORD.+' "$WP_CONFIG" | cut -d"'" -f 4)
	DB_HOST=$(grep -o -E '^[[:space:]]*define.+?DB_HOST.+?,[[:space:]]*.+?[0-9a-zA-Z_\.]*' "$WP_CONFIG" | cut -d"'" -f 4)
	DB_TABLE_PREFIX=$(grep -o -E '^[[:space:]]*\$table_prefix[[:space:]]*=[[:space:]]*.*' "$WP_CONFIG" | cut -d"'" -f 2)
	BACKUP_NAME_TGZ="${DB_NAME}_$BACKUP_NAME.tar.gz"

	if [ 'true' == "$DO_MIGRATION" ]
	then
		read -p "Please enter the database username for the new site [$DB_USER]: " DB_USER_NEW
		if [ "" == "$DB_USER_NEW" ]
		then
			if [ "" == "$DB_USER" ]
			then
				echo "We're sorry but the database username cannot be left blank."
				echo "No action has been taken."
				exit 104
			else
				DB_USER_NEW="$DB_USER"
			fi
		fi
	else
		if [ "" == "$DB_USER" ]
		then
			echo "We're sorry but the database username cannot be left blank."
			echo "No action has been taken."
			exit 104
		fi
	fi
	
	
	if [ 'true' == "$DO_MIGRATION" ]
	then
		read -p "Please enter the password for this site [$DB_PASS]: " DB_PASS_NEW
		# if the password is empty, as could be the case for insecure 
		# servers, don't use the -p switch
		if [ "" == "$DB_PASS_NEW" ]
		then
			if [ "" == "$DB_PASS" ]
			then
				PASS_NEW=''
			else
				echo "The password is NOT empty, this is good!"
				PASS_NEW="$DB_PASS"
			fi
		else
			PASS_NEW="$DB_PASS_NEW"
		fi
	fi
	
	if [ "" == "$DB_PASS" ]
	then
		PASS=''
	else
		echo "The password is NOT empty, this is good!"
		PASS="-p'$DB_PASS'"
	fi
	
	if [ 'true' == "$DO_MIGRATION" ]
	then
		read -p "Please enter the database name for this site [$DB_NAME]: " DB_NAME_NEW
		if [ "" == "$DB_NAME_NEW" ]
		then
			if [ "" == "$DB_NAME" ]
			then
				echo "We're sorry but the database name cannot be blank."
				echo "No action has been taken."
				exit 105
			else
				DB_NAME_NEW="$DB_NAME"
			fi
		fi
	else
		if [ "" == "$DB_NAME" ]
		then
			echo "We're sorry but the database name cannot be left blank."
			echo "No action has been taken."
			exit 105
		fi
	fi
	
	if [ 'true' == "$DO_MIGRATION" ]
	then
		read -p "Please enter the database host for this site [$DB_HOST]: " DB_HOST_NEW
		if [ "" == "$DB_HOST_NEW" ]
		then
			if [ "" == "$DB_HOST" ]
			then
				echo "We're sorry but the database host cannot be blank."
				echo "No action has been taken."
				exit 106
			else
				DB_HOST_NEW="$DB_HOST"
			fi
		fi
	else
		if [ "" == "$DB_HOST" ]
		then
			echo "We're sorry but the database host cannot be left blank."
			echo "No action has been taken."
			exit 106
		fi
	fi
	
	HOST_HAS_PORT=$(echo $DB_HOST_NEW | grep -o ':')
	if [ ! "" == "$HOST_HAS_PORT" ]
	then
		DB_HOST_NEW=${DB_HOST_NEW/\:[0-9]*/}
	fi
	
	if [ "" == "$DB_TABLE_PREFIX" ]
	then
		echo "Either the table prefix is blank or we were unable to find it."
		read -p "Please enter the table prefix for this site and hit Enter: " DB_TABLE_PREFIX
		if [ "" == "$DB_TABLE_PREFIX" ]
		then
			echo "It's unusual for the table prefix to be completely "
			echo "blank, but not impossible."
			echo "We'll give it a whirl regardless. Proceeding..."
		fi
	fi
	
	# from this point on I give up on trying to keep lines under 72 chrs
	# dump the db to a file inside the web root
	# there is some kind of bizzare issue porhibiting me from quoting the value of the password parameter
	# as a workaround, I'm going to try writing the command to an external file and executing it instead of trying to run it here
	echo '#!/bin/sh' > "$MYSQLDCOMMAND"
	echo "
	
	" >> "$MYSQLDCOMMAND"
	# not really sure how to capture a non-zero exit status from the command below
	echo "mysqldump --lock-tables -u '$DB_USER' $PASS -h $DB_HOST '$DB_NAME' > '$DB_BACKUP_FILE'" >> "$MYSQLDCOMMAND"
	. "$MYSQLDCOMMAND" 2>> "$BACKUP_DIR/backup_error.log"

	if [ -f "$DB_BACKUP_FILE" ]
	then
		echo "The backup file was created. Checking to see if it is valid."
		# do a sanity check
		#	make sure it is a minimum size
		DB_BACKUP_FILE_SIZE=$(du -k "$DB_BACKUP_FILE" | cut -f 1) 2>> "$BACKUP_DIR/backup_error.log"
		if [ "$DB_BACKUP_FILE_SIZE" -lt 84 ]
		then
			# file size is too small
			echo "The size of the database dump seems suspiciously small ($DB_BACKUP_FILE_SIZE). Please check the size and try again."
			exit 102
		else
			echo "The backup file seems to be large enough to be valid"
		fi
		#	make sure some key tables are defined
		# grep the backup file looking for definitions of default tables
		# if all the tables are there (as judged by the number of lines found, S/B 11) then proceed
		TABLES=$(grep -c -E 'CREATE TABLE `'${DB_TABLE_PREFIX}'(commentmeta|comments|links|options|postmeta|posts|term_relationships|term_taxonomy|termmeta|terms|usermeta|users)`' "$DB_BACKUP_FILE") 2>> "$BACKUP_DIR/backup_error.log"
		if [ $TABLES -lt 11 ]
		then
			echo "The backup seems to have failed. Specifically, we seem to be missing some core tables in the database backup. Please check the file and try again."
			echo "$DB_BACKUP_FILE"
			exit 103
		else
			echo "All required tables appear to be defined in the dump"
		fi
		echo "All tests have passed. Proceeding..."
		
		if [ 'true' == "$DO_MIGRATION" ]
		then
			echo "Preparing the database for migration to a new server."
			# get the old Site and Home URLs
			# (1,'siteurl','http://newmoneytree.local/~tedsr/isluk_v2/','yes'),
			# (33,'home','http://newmoneytree.local/~tedsr/isluk_v2/','yes'),
			OLD_SITE_URL=$(grep -o -E "\([0-9]+,'siteurl','.+?','(yes|no)'\)," "$DB_BACKUP_FILE" | cut -d"'" -f 4) 2>> "$BACKUP_DIR/backup_error.log"
			OLD_HOME_URL=$(grep -o -E "\([0-9]+,'home','.+?','(yes|no)'\)," "$DB_BACKUP_FILE" | cut -d"'" -f 4) 2>> "$BACKUP_DIR/backup_error.log"
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed "s@$OLD_SITE_URL@$NEW_SITE_URL@g") 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed "s@$OLD_HOME_URL@$NEW_HOME_URL@g") 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			echo "Updated references to $OLD_SITE_URL and $OLD_HOME_URL to $NEW_SITE_URL and $NEW_HOME_URL."
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed "s@$THIS_DIR/$PUBLIC_HTML@$NEW_SITE_PATH@g") 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			
			# At this point all paths and URLs have been updated but if 
			# the theme contains options as serialized arrays or strings
			# the update will fail. So, we need to update the string 
			# lengths in the serialized arrays before we can proceed.
			# a:5:{s:3:"url";s:80:"http://localhost/~tedsr/rosa2test/wp-content/uploads/2014/06/logo-rosa-white.png";s:2:"id";s:3:"200";s:6:"height";s:2:"28";s:5:"width";s:2:"86";s:9:"thumbnail";s:80:"http://localhost/~tedsr/rosa2test/wp-content/uploads/2014/06/logo-rosa-white.png";}
			# The same is true for URLs stored as escaped strings
			
			# replace references to utf8mb4 to just utf8
			# See this bug report for details: https://secretsource.atlassian.net/browse/SECRETSOUR-46
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed 's/CHARACTER SET utf8mb4/CHARACTER SET utf8/g') 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed 's/COLLATE utf8mb4_unicode_ci/COLLATE utf8_unicode_ci/g') 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed 's/COLLATE=utf8mb4_unicode_ci/COLLATE=utf8_unicode_ci/g') 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			NEW_SQL=$(cat "$DB_BACKUP_FILE" | sed 's/CHARSET=utf8mb4/CHARSET=utf8/g') 2>> "$BACKUP_DIR/backup_error.log"
			echo "$NEW_SQL" > "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
			
			echo "Updating wp-config.php to use the new values."
			mkdir -p "$BACKUP_DIR/temp"
			# make a backup of the existing wp-config so as not to overwrite
			cp "$THIS_DIR/$PUBLIC_HTML/wp-config.php" "$BACKUP_DIR/temp/wp-config.php"
			NEW_WP_CONFIG=$(cat "$WP_CONFIG")
			if [ ! "$DB_USER" == "$DB_USER_NEW" ]
			then
				NEW_WP_CONFIG=$(echo "$NEW_WP_CONFIG" | sed "s@^define *( *'DB_USER', *'$DB_USER'@define('DB_USER', '$DB_USER_NEW'@g") 2>> "$BACKUP_DIR/backup_error.log"
			fi
			if [ ! "$DB_PASS" == "$DB_PASS_NEW" ]
			then
				# this fails if the password contains an at symbol, maybe some kind bash guru will lend a hand here?
				ESCAPED_DB_PASS=$(echo $DB_PASS | sed 's/[\/&]/\\&/g')
				ESCAPED_DB_PASS_NEW=$(echo $DB_PASS_NEW | sed 's/[]\/$*.^|[]/\\&/g')
				NEW_WP_CONFIG=$(echo "$NEW_WP_CONFIG" | sed "s/^define *( *'DB_PASSWORD', *'$ESCAPED_DB_PASS'/define('DB_PASSWORD', '$ESCAPED_DB_PASS_NEW'/g") 2>> "$BACKUP_DIR/backup_error.log"
			fi
			if [ ! "$DB_NAME" == "$DB_NAME_NEW" ]
			then
				NEW_WP_CONFIG=$(echo "$NEW_WP_CONFIG" | sed "s@^define *( *'DB_NAME', *'$DB_NAME'@define('DB_NAME', '$DB_NAME_NEW'@g") 2>> "$BACKUP_DIR/backup_error.log"
			fi
			if [ ! "$DB_HOST" == "$DB_HOST_NEW" ]
			then
				NEW_WP_CONFIG=$(echo "$NEW_WP_CONFIG" | sed "s@^define *( *'DB_HOST', *'$DB_NAME'@define('DB_HOST', '$DB_HOST_NEW'@g") 2>> "$BACKUP_DIR/backup_error.log"
			fi
			echo "$NEW_WP_CONFIG" > "$WP_CONFIG"
		fi
		
		# remove ennecessary readmes, licenses, and error_log files, fixddbb.php in addition to .DS_Store and ._filename files (usually invisible files on a mac)
		if [ 'true' == "$DO_MIGRATION" ]
		then
			for F in $(find -E . -iregex '$THIS_DIR/$PUBLIC_HTML/.*((readme|license)(\.(htm(l)?|txt))*|error_log|fixddbb\.php)$' -type f)
			do
				echo "Removing cruft: $F"
				[ "$F" ] && rm -f "$F"
			done
		fi
		
		if [ 'true' == "$SKIP_UPLOADS" ]
		then
		    EXCLUDE="--exclude=./wp-content/uploads"
		else
		    EXCLUDE=""
		fi
		tar -czf "$BACKUP_DIR/$BACKUP_NAME_TGZ" -C "$THIS_DIR/$PUBLIC_HTML" "$EXCLUDE" . 2>> "$BACKUP_DIR/backup_error.log"

		echo "Cleaning up..."
		if [ 'true' == "$DO_MIGRATION" ]
		then
			cp "$BACKUP_DIR/temp/wp-config.php" "$THIS_DIR/$PUBLIC_HTML/wp-config.php" 2>> "$BACKUP_DIR/backup_error.log"
		fi
		[ "$MYSQLDCOMMAND" ] && rm -f "$MYSQLDCOMMAND" 2>> "$BACKUP_DIR/backup_error.log"
		[ "$DB_BACKUP_FILE" ] && rm -f "$DB_BACKUP_FILE" 2>> "$BACKUP_DIR/backup_error.log"
		echo "Done. The backup file is: "
		echo "$BACKUP_DIR/$BACKUP_NAME_TGZ"
		BACKUPFILESIZE=$(ls -lha "$BACKUP_DIR/$BACKUP_NAME_TGZ" | awk '{print $5}')
		echo "The backup file is $BACKUPFILESIZE"
		BACKUPFOLDERSIZE=$(du -h $DEPTH "$BACKUP_DIR")
		echo "The backup folder size is $BACKUPFOLDERSIZE"
		
		# remove anything older than TAIL days
        # only worry about the tail if it is not empty
        if [[ ! "" == "$TAIL" && (( $TAIL > 0 )) ]]
        then
            # count the number of backups and act if greater than TAIL
            TOTAL_BACKUPS=$(ls -rt "$BACKUP_DIR/" | grep "_backup_" | wc -l  | sed 's/ //g')
            while (( $TOTAL_BACKUPS > $TAIL ))
            do
                # delete the oldest files
                FILE2DELETE=$(ls -rt "$BACKUP_DIR/" | head -n 1)
                echo "Deleting $BACKUP_DIR/$FILE2DELETE"
                [ "$BACKUP_DIR/$FILE2DELETE" ] && rm -rf "$BACKUP_DIR/$FILE2DELETE"
                TOTAL_BACKUPS=$(ls -rt "$BACKUP_DIR/" | grep "_backup_" | wc -l  | sed 's/ //g')
            done
            BACKUPFOLDERSIZE=$(du -h $DEPTH "$BACKUP_DIR")
            echo "The backup folder is now $BACKUPFOLDERSIZE"
        fi

	else
		echo "backup file doesn't exist"
	fi
else
	# echo an error message and quit
	echo "Uh, the wp-config.php file can't be found"
	echo "We're looking here for it: $WP_CONFIG"
	echo "Are you running this script from the right location? ~/"
	echo "Have gremlins eaten it?"
	exit 101
fi
exit 0
