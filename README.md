# README #

`ssbackup.sh` and `ssrestore.sh` are two scripts designed to facilitate the process of updating, backing up, and restoring WordPress web sites.

In its simplest form, these two scripts can be used to back up a site by executing `ssbackup.sh` and to restore `ssrestore.sh` from the command line.

`ssbackup.sh` understands a few flags including `--migrate` and `--tail`. Using the `--migrate` flag, you can create a package that can be restored on a different server, under a different domain, and with different database credentials. `--tail` allows you to specify the maximum number of backup files in the backups folder. This is particularly useful when running `ssbackup.sh` from cron (scheduled task) as it allows you to minimize the amount of storage used on your system. This is significant because this script will back up your entire WordPress installation, often hundreds of MB and sometimes more than 1 GB.

Lastly, this is very much a work-in-progress with known issues. Please, if you find an issue, and have a fix, please send it my way via a "pull request". Thanks in advance.


# LICENSE #
Released without warranty under GPLv2

# IMPORTANT #
The scripts assume a few things that, if not totally kosher, may have unexpected consequences. This is what the script(s) assumes:

1. The user you log in as has write access to the directory above the DOCUMENT_ROOT. So, for example, if you're on a shared host logged in as your own user, you can probably write files to ~/SecretSourceBackups. This is sufficient access.
2. You will run the script from the directory just above your DOCUMENT_ROOT. So, if you log in to your account via SSH on a shared host, you'll run the scripts from a directory that has a subfolder called "public_html".
3. You are NOT doing multisite WordPress (maybe I'll write something for that some day…)

# An Example (is worth a bunch of screenshots) #

We are going to update the fictitious site fancywidgets.com. Fancywidgets.com is a WooCommerce store with a blog and all kinds of social plugins and masonry galleries and every manner of hook and theme modification you can imagine. The update is going to fail!

For this example we assume these two scripts are installed on the server (probably in /usr/local/bin, but could be anywhere a normal user can run them).

These are the steps we follow to update a site:

1. Log on to the server via SSH.
2. Navigate to the directory just above your DOCUMENT_ROOT. On a shared server you probably won't have do to anything as the default is to land in your home directory, which usually has public_html as a subdirectory.
3. Run ssbackup.sh. The script will create a complete backup of your database and site files.
4. Log in to the WordPress Admin and run the updaters.
5. Go to the front end of your web site and soak up the joy of open source software (that's sarcasm in case you hadn't noticed)
6. From the command line, run ssrestore.sh. You'll see a list of backup files. Pick one, probably the most recent, and hit Enter to roll back.
7. Figure out how to fix all the broken pieces and do steps 3 through 6 ad infinitum.

### Here are some example commands: ###

Your DOCUMENT_ROOT is not called "public_html" but rather "web"
```
ssbackup.sh --documentroot 'web'
```

Your DOCUMENT_ROOT is not called "public_html" but rather "web" and is located in a different directory than your current location
```
ssbackup.sh --documentroot '~/webappversion1/web'
```

You want to limit the number of backups to 14 (this could be added to your crontab and run nightly)
```
ssbackup.sh --tail 14
```

You want to prepare a backup for restoration on another server and under a different domain (a production server, for example)
```
ssbackup.sh --migrate
```


## Experimental - Use At Your Own Risk ##

Now that we've got a "packaging" system, it only makes sense to use it to migrate sites from local dev machines to staging and production servers. You can do that by passing the --migrate flag to the backup script and following the prompts.

```
ssbackup.sh --migrate
```

When you've made your backup, copy it to the SecretSourceBackups folder on the destination server, log in via SSH on the destination server, and  run `ssrestore.sh`. You will then be prompted to select a package from a  list of available backups to "restore" your wordpress installation. Please note, YOUR ENTIRE DOCUMENT_ROOT FOLDER WILL BE OVERWITTEN WITH THIS COMMAND AND YOU WILL LOSE ALL DATA. Please understand what you are doing prior to running this command this way. (actually, prior to restoring, the command will make a backup of your existing installation just in case… but still).

Good luck!

Ted
http://secret-source.eu

Going forward I want to start using wp-cli to improve the migration of theme options:
wp search-replace 'http://localhost/' 'http://newmoneytree.local/' --all-tables --verbose